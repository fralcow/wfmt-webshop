package main

import (
	"errors"
	"os"
	"strings"
	"time"

	"github.com/dgrijalva/jwt-go"
	"golang.org/x/crypto/bcrypt"
)

func GenerateBearer(account, userType string) (tokenString string, err error) {
	token := jwt.New(jwt.SigningMethodHS256)
	claims := token.Claims.(jwt.MapClaims)

	claims["authorized"] = true
	claims["account"] = account
	claims["type"] = userType
	claims["exp"] = time.Now().Add(time.Minute * 30).Unix()

	tokenString, err = token.SignedString([]byte(os.Getenv("JWT_SECRET")))
	if err != nil {
		return
	}
	return
}

func authenticateUser(account, password string) (user User, err error) {
	db, err := DB()
	if err != nil {
		return
	}

	user = User{Account: account}
	err = db.Where("account = ?", account).First(&user).Error
	if err != nil {
		return
	}

	err = bcrypt.CompareHashAndPassword([]byte(user.PasswordHash), []byte(password))
	if err != nil {
		return
	}

	return
}

func validateToken(t string) (token *jwt.Token, err error) {
	hasPrefix := strings.HasPrefix(t, "Bearer ")
	if !hasPrefix {
		return nil, errors.New("Error decoding token")
	}

	t = strings.TrimPrefix(t, "Bearer ")

	claims := jwt.MapClaims{}
	token, err = jwt.ParseWithClaims(t, claims, func(token *jwt.Token) (interface{}, error) {
		if _, ok := token.Method.(*jwt.SigningMethodHMAC); !ok {
			return nil, errors.New("Error decoding token.")
		}
		return []byte(os.Getenv("JWT_SECRET")), nil
	})
	if err != nil {
		return
	}

	return
}
