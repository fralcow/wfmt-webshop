package main

import (
	"fmt"
	"os"

	"github.com/pkg/errors"
	log "github.com/sirupsen/logrus"
	"golang.org/x/crypto/bcrypt"
	"gorm.io/driver/postgres"
	"gorm.io/gorm"
	"gorm.io/gorm/clause"
	"gorm.io/gorm/logger"
)

var webshopDB *gorm.DB
var webshopDBConfig *gorm.Config

type User struct {
	gorm.Model
	Account      string `gorm:"unique"`
	PasswordHash []byte `gorm:"not null"`
	Type         string `gorm:"not null"`
}

type Product struct {
	gorm.Model
	Name        string `gorm:"unique"`
	Description string
	Price       float32
	Amount      int
}

type Cart struct {
	gorm.Model
	User     User
	Products []Product
}

func DB() (*gorm.DB, error) {
	if webshopDB != nil {
		return webshopDB, nil
	}
	PostgresName := os.Getenv("POSTGRES_DB")
	PostgresUser := os.Getenv("POSTGRES_USER")
	PostgresPassword := os.Getenv("POSTGRES_PASSWORD")
	PostgresHost := os.Getenv("POSTGRES_HOST")
	PostgresPort := os.Getenv("POSTGRES_PORT")
	dsn := fmt.Sprintf("host=%v user=%v password=%v dbname=%v port=%v sslmode=disable", PostgresHost, PostgresUser, PostgresPassword, PostgresName, PostgresPort)
	log.Debugf("dsn: %v", dsn)

	log_level, err := log.ParseLevel(os.Getenv("LOG_LEVEL"))
	if webshopDBConfig == nil {
		if log_level == log.DebugLevel {
			webshopDBConfig = &gorm.Config{}
		} else {
			webshopDBConfig = &gorm.Config{
				Logger: logger.New(
					log.New(), logger.Config{LogLevel: logger.Silent},
				),
			}
		}
	}

	log.Debugf("Gorm config: %+v", webshopDBConfig)
	db, err := gorm.Open(postgres.Open(dsn), webshopDBConfig)
	if err != nil {
		return nil, errors.Wrap(err, "Failed to connect to database")
	}
	webshopDB = db

	return webshopDB, err
}

func setupDatabase() (err error) {
	account := os.Getenv("MANAGER_USERNAME")

	p := os.Getenv("MANAGER_PASSWORD")
	hash, err := bcrypt.GenerateFromPassword([]byte(p), bcrypt.DefaultCost)
	if err != nil {
		return err
	}

	user := User{
		Account:      account,
		PasswordHash: hash,
		Type:         "manager",
	}

	db, err := DB()
	if err != nil {
		return err
	}

	tx := db.Clauses(clause.OnConflict{
		Columns:   []clause.Column{{Name: "account"}},
		DoUpdates: clause.AssignmentColumns([]string{"password_hash"}),
	}).Create(&user)
	if tx.Error != nil {
		return tx.Error
	}

	return
}

func DBSetConfig(c *gorm.Config) {
	webshopDBConfig = c
}
