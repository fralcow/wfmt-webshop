package main

import (
	"os"

	log "github.com/sirupsen/logrus"
	"github.com/spf13/viper"
)

func setEnvVars() (err error) {
	if os.Getenv("DOCKER") != "true" {
		log.Debug("Setting NOT docker env vars")
		err = os.Setenv("POSTGRES_DB", viperEnvVariable("POSTGRES_DB"))
		err = os.Setenv("POSTGRES_USER", viperEnvVariable("POSTGRES_USER"))
		err = os.Setenv("POSTGRES_PASSWORD", viperEnvVariable("POSTGRES_PASSWORD"))
		err = os.Setenv("POSTGRES_HOST", viperEnvVariable("POSTGRES_HOST"))
		err = os.Setenv("POSTGRES_PORT", viperEnvVariable("POSTGRES_PORT"))
		err = os.Setenv("JWT_SECRET", viperEnvVariable("JWT_SECRET"))
		err = os.Setenv("LOG_LEVEL", viperEnvVariable("LOG_LEVEL"))
		err = os.Setenv("MANAGER_USERNAME", viperEnvVariable("MANAGER_USERNAME"))
		err = os.Setenv("MANAGER_PASSWORD", viperEnvVariable("MANAGER_PASSWORD"))
		err = os.Setenv("GIN_MODE", viperEnvVariable("GIN_MODE"))
	}
	return err
}

func viperEnvVariable(key string) string {

	viper.SetConfigFile(".env")
	err := viper.ReadInConfig()
	if err != nil {
		log.Fatalf("Error while reading config file %s", err)
	}

	value, ok := viper.Get(key).(string)
	if !ok {
		log.Fatalf("Invalid type assertion, key %v", key)
	}

	return value
}
