package main

import (
	"os"

	log "github.com/sirupsen/logrus"
)

func setupLogs() {
	log_level, err := log.ParseLevel(os.Getenv("LOG_LEVEL"))
	if err != nil {
		log_level = log.InfoLevel
		log.Warnf("Failed to parse LOG_LEVEL: %v. Fallback to \"%v\".", err, log_level)
	}

	log.SetLevel(log_level)
}
