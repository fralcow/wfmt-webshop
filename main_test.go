package main

import (
	"flag"
	"fmt"
	"net/http"
	"net/http/httptest"
	"os"
	"testing"

	"github.com/go-playground/assert/v2"
	"github.com/golang-migrate/migrate"
	"github.com/ory/dockertest"
	"github.com/ory/dockertest/docker"
	"github.com/stretchr/testify/suite"
	"gorm.io/gorm"
	"gorm.io/gorm/logger"

	log "github.com/sirupsen/logrus"
)

var (
	user     = "postgres"
	password = "secret"
	db_name  = "postgres"
	host     = "localhost"
	port     = "5440"
)

type WebshopTestSuite struct {
	suite.Suite
}

var db *gorm.DB

func TestMain(m *testing.M) {
	var debug = flag.Bool("debug", false, "Set log level debug")
	flag.Parse()
	log.SetLevel(log.InfoLevel)
	if *debug {
		log.SetLevel(log.DebugLevel)
	}
	pool, err := dockertest.NewPool("")
	if err != nil {
		log.Fatalf("Could not connect to docker: %s", err)
	}

	opts := dockertest.RunOptions{
		Repository: "postgres",
		Tag:        "13.1",
		Env: []string{
			"POSTGRES_USER=" + user,
			"POSTGRES_PASSWORD=" + password,
			"POSTGRES_DB=" + db_name,
		},
		ExposedPorts: []string{"5432"},
		PortBindings: map[docker.Port][]docker.PortBinding{
			"5432": {
				{HostIP: "0.0.0.0", HostPort: port},
			},
		},
	}

	resource, err := pool.RunWithOptions(&opts)
	if err != nil {
		log.Fatalf("Could not start resource: %s", err.Error())
	}

	// set env vars
	os.Setenv("POSTGRES_DB", db_name)
	os.Setenv("POSTGRES_USER", user)
	os.Setenv("POSTGRES_PASSWORD", password)

	testDatabaseHost := os.Getenv("YOUR_APP_DB_HOST")
	if len(testDatabaseHost) > 0 {
		host = testDatabaseHost
	}
	os.Setenv("POSTGRES_HOST", host)

	os.Setenv("POSTGRES_PORT", port)
	if err := pool.Retry(func() error {
		config := &gorm.Config{
			DisableForeignKeyConstraintWhenMigrating: true,
			Logger: logger.New(
				log.New(), logger.Config{LogLevel: logger.Silent},
			),
		}

		if *debug {
			config = &gorm.Config{
				DisableForeignKeyConstraintWhenMigrating: true,
			}
		}

		DBSetConfig(config)
		db, err = DB()
		if err != nil {
			log.Println("Database not ready yet (it is booting up, wait for a few tries)...")
			return err
		}

		// Tests if database is reachable
		sqldb, err := db.DB()
		if err != nil {
			log.Println("Database is not reachable")
			return err
		}

		return sqldb.Ping()
	}); err != nil {
		log.Fatalf("Could not connect to Docker: %s", err)
	}

	// Run the actual test cases (functions that start with Test...)
	code := m.Run()

	if err := pool.Purge(resource); err != nil {
		log.Fatalf("Could not purge resource: %s", err)
	}

	os.Exit(code)

}

func TestEconomy(t *testing.T) {
	suite.Run(t, new(WebshopTestSuite))
}

func (suite *WebshopTestSuite) SetupSuite() {
}

func (suite *WebshopTestSuite) TearDownSuite() {
}

func subtestSetup() {
	log.Debug("Running subtest setup...")
	dburl := fmt.Sprintf("postgres://%v:%v@%v:%v/%v?sslmode=disable", user, password, host, port, db_name)
	m, err := migrate.New("file://../../postgres/migrations", dburl)
	if err != nil {
		log.Fatalf("Failed to initialize test db: %s", err)
	}

	if err := m.Up(); err != nil {
		log.Fatal(err)
	}

}

func subtestTeardown() {
	log.Debug("Run teardown...")
	dburl := fmt.Sprintf("postgres://%v:%v@%v:%v/%v?sslmode=disable", user, password, host, port, db_name)
	m, err := migrate.New("file://../../postgres/migrations", dburl)
	if err != nil {
		log.Fatalf("Failed to initialize test db: %s", err)
	}

	if err := m.Drop(); err != nil {
		log.Fatal(err)
	}
}

func (suite *WebshopTestSuite) TestGetProducts() {
	t := suite.T()

	router := setupRouter()

	w := httptest.NewRecorder()
	req, _ := http.NewRequest("GET", "/v1/products", nil)
	router.ServeHTTP(w, req)

	assert.Equal(t, 401, w.Code)
}
