BEGIN;

CREATE TABLE public.carts (
    id bigint NOT NULL,
    created_at timestamp with time zone,
    updated_at timestamp with time zone,
    deleted_at timestamp with time zone,
    user_id bigint NOT NULL
);

ALTER TABLE public.carts OWNER TO krup;

CREATE SEQUENCE public.carts_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.carts_id_seq OWNER TO krup;

ALTER SEQUENCE public.carts_id_seq OWNED BY public.carts.id;

ALTER TABLE ONLY public.carts ALTER COLUMN id SET DEFAULT nextval('public.carts_id_seq'::regclass);

ALTER TABLE ONLY public.carts
    ADD CONSTRAINT carts_pkey PRIMARY KEY (id);

CREATE INDEX idx_carts_deleted_at ON public.carts USING btree (deleted_at);

ALTER TABLE ONLY public.carts
    ADD CONSTRAINT fk_users_carts FOREIGN KEY (user_id) REFERENCES public.users(id) ON DELETE CASCADE;

COMMIT;
