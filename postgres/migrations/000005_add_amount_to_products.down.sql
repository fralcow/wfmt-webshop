BEGIN;

ALTER TABLE ONLY public.products DROP COLUMN amount;

COMMIT;
