BEGIN;

ALTER TABLE public.products
ADD COLUMN amount bigint;

COMMIT;
